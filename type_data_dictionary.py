# seperti objeck di js

kamus = {}
kamus['anak'] = 'son'
kamus['ibu'] = 'mother'
kamus['bapak'] = 'father'
print(kamus['ibu'])

# data gojek misal
print('data ini dikirimkan oleh server')
data_gojek = {
    'tanggal': '2020-01-15',
    'nama': 'vandy',
    'daftar_semua_driver': [
        {'nama': 'vandy', 'jarak': 100},
        {'nama': 'bambang', 'jarak': 30},
        {'nama': 'ahmad', 'jarak': 10}
    ]
}
print(data_gojek['daftar_semua_driver'][1]['jarak'])
